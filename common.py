from urllib.request import Request, urlopen
from bs4 import BeautifulSoup as soup


def search_query(search_url, query):
    #read html page
    page_soup = read_page(search_url)

    #output array
    data = []
    top_class = ["gtm-top-ad", "gtm-normal-ad"]

    for i in range(len(top_class)) :
        top_containers = find_all_content(page_soup,"li","class",top_class[i])
        for top_container in top_containers:
            res = {}
            
            card_link = find_all_content(top_container,"a","class","gtm-ad-item")
            for link in card_link:
                url = "https://ikman.lk" + link.get('href')
                title = link.get('title')
            
            res['title'] = title
            res['url'] = url
            pageTitle = page_soup.find('h1').get_text()
            res["category"] = pageTitle

            #read html page
            inner_page_soup = read_page(url)

            #get inner images
            image_array = find_all_content(inner_page_soup,"img","alt",title)
            image_url = get_image(image_array)
    
            long_des = find_all_content(inner_page_soup,"span","class","poster")
            full_des = format_values(long_des) 
            
            contact_array = find_all_content(inner_page_soup,"div","class","item-contact-more is-showable")
            contact_no = format_values(contact_array)  
            
            ad_date_final = find_all_content(inner_page_soup,"span","class","date")
            res['date'] = format_values(ad_date_final)

            #get outtter content
            title_container = find_all_content(top_container,"div","class","content--3JNQz")
            for title in title_container:
                detail_other = title.div
                sub_title = find_all_content(detail_other,"div","class","sub-title--1nbZO")
                area = sub_title[0].text.strip()
                res['short_decription'] = area
                price = find_all_content(detail_other,"div","class","price--3SnqI")
                price_final = price[0].text.strip()
                detail_array = {}
                detail_array['full_description'] = full_des
                detail_array['price'] = price_final
                detail_array['contact'] = contact_no
                detail_array['image_urls'] = image_url
                res['details'] = detail_array
                data.append(res)

    return data

def read_page(url):
    #open connection and grab the page
    url = Request(url)
    urlClient = urlopen(url)
    page_html = urlClient.read()
    urlClient.close()

    #html parsing
    page_soup = soup(page_html, "html.parser")
    return page_soup

def get_image(image_array):
    image_url = []
    for image in image_array:
        if image.has_attr("src") :
            g_image = image['src']
            image_url.append(g_image)
    
    return image_url

def format_values(value):
    if value != [] :
        retrun_value = value[0].text.strip()
    else:
        retrun_value = "-"
    return retrun_value

def find_all_content(page,tag,item,feature) :
    result = page.findAll(tag,{item:feature})
    return result
                