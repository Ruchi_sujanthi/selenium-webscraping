from selenium import webdriver
from selenium.webdriver.common.by import By
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC
from selenium.common.exceptions import TimeoutException
from bs4 import BeautifulSoup as soup

option = webdriver.ChromeOptions()
option.add_argument("--incognito")

browser = webdriver.Chrome(executable_path='./chromedriver', chrome_options=option)
browser.get("https://ikman.lk/en/ads")

# Wait 3600 seconds for page to load
timeout = 3600
try:
    # Wait until the final element [Avatar link] is loaded.
    # Assumption: If Avatar link is loaded, the whole page would be relatively loaded because it is among
    # the last things to be loaded.
    WebDriverWait(browser, timeout).until(EC.visibility_of_element_located((By.XPATH, "//img[@class='avatar width-full rounded-2']")))
except TimeoutException:
    print("Timed out waiting for page to load")
    browser.quit()

# find_elements_by_xpath - Returns an array of selenium objects.
titles_element = browser.find_element_by_class_name('category-selector')

# List Comprehension to get the actual repo titles and not the selenium objects.
titles = [x.text for x in titles_element]

# print response in terminal
print('TITLES:')
print(titles, '\n')


search_field = browser.find_element_by_class_name('1gFez')
search_field.send_keys('smartphone' + '\n')
# Here time.sleep is used to add delay for loading context in browser
time.sleep(2)
# Here we fetched driver page source from driver.
page_html = browser.page_source
# Here BeautifulSoup is dump page source into html format
inner_html = soup(page_html, 'html.parser')
print(inner_html)